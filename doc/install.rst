.. _install:

==========
Installing
==========

.. code-block:: bash

    $ pip3 install merlin_interface

How to use: :ref:`tutorial`.
