### Webpage: https://fast_pixelated_detectors.gitlab.io/merlin_interface/

# Merlin interface

Simple python wrapper for interfacing with the Merlin Medipix readout software through the TCP/IP API.
